# About this repository #
This repository contains a collection of modules and scripts written in lovely Haskell.

Feel free to contact me if you find any mistakes in my code and also if you want me to know something else like
"Why haven't you implement feature X?".

Does it seem like a nice idea to me I'll try to do my best and improve my code
by implementing your suggestions.

# Wiki #
In the [Wiki](https://bitbucket.org/ROYALBEFF/haskell-modules/wiki/Home) you will find an overview of all modules and scripts including a small description.

# License #
All code you'll find in this repository is distributed under [GNU GPLv3](/LICENSE).