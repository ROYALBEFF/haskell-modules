module SimpleRegex.Parser(parse) where

import SimpleRegex

data RegG = Start RegG |
            Mul RegG -> RegG |
            Plus RegG -> RegG |
            Enclose RegG |
            Iterate RegG |
            Embed Char

parse :: String -> Regex Char
parse str = case parseReg [] str of
    Just ([reg], "")    -> reg
    _                   -> Null

parseReg :: [Regex Char] -> String -> Maybe ([Regex Char], String)
parseReg reg "" = Just (reg, "")
parseReg [] ('+':_) = Nothing
parseReg [reg] ('+':rs) =
    parseReg [] rs >>= \(reg', rs') -> parseReg [reg :+ head reg'] rs'
parseReg [] ('*':_) = Nothing
parseReg [reg] ('*':rs) = parseReg [(Iter reg)] rs
parseReg [] ('(':rs) = parseReg [] rs
parseReg [reg] ('(':rs) =
    parseReg [] rs >>= \(reg', rs') -> parseReg [(reg :. head reg')] rs'
parseReg reg (')':rs) = Just (reg, rs)
parseReg [] (r:rs) = parseReg [Sym r] rs
parseReg [reg] (r:rs) = parseReg [(reg :. Sym r)] rs
