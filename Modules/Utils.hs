module Utils(
    (...),
    (....),
    update,
    update2,
    restrict,
    include,
    prod,
    fprod,
    coprod,
    btn
) where

-- | Allows to compose two functions with the second function applied can take
--   two parameters instead of just one.
(...) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
(...) = (.) . (.)

-- | Allows to compose two functions with the second function applied can take
--   three parameters instead of just one.
(....) :: (d -> e) -> (a -> b -> c -> d) -> a -> b -> c -> e
(....) = (...) . (.)

-- | Updates a function by changing the result value for a from f (a) to b.
--   For all other input values f works as before.
update :: Eq a => (a -> b) -> a -> b -> a -> b
update f a b = \x -> if x == a then b else f x

-- | Like update. Updates a function that takes two parameters.
update2 :: (Eq a, Eq b) => (a -> b -> c) -> a -> b -> c -> a -> b -> c
update2 f a b c = \x y -> if x == a && y == b then c else f x y

-- | Restricts the function f such that it will only work on the subset c of a.
restrict :: Eq a => (a -> b) -> [a] -> a -> Maybe b
restrict f c = \x -> if elem x c then Just $ f x else Nothing

-- | Restricts the id function.
include :: Eq a => [a] -> a -> Maybe a
include = restrict id

-- | Cartesian product of sets s and s'.
prod :: [a] -> [b] -> [(a,b)]
prod s s' = (,) <$> s <*> s'

-- | Cartesian product of sets s and s' as a set of functions.
fprod :: [a] -> [b] -> [Int -> Either a b]
fprod s s' = f <$> s <*> s' where
    f :: a -> b -> Int -> Either a b
    f a _ 1 = Left a
    f _ b 2 = Right b

-- | Coproduct (disjunct union) of sets s and s'.
coprod :: [a] -> [b] -> [Either a b]
coprod s s' = (Left <$> s) ++ (Right <$> s')

-- | Transforms a boolean value to 0 or 1.
btn :: Num a => Bool -> a
btn b = if b then 1 else 0
