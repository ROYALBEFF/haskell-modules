module ANN.RBFN(
    gauss,
    classify
) where

import Utils((...), (....))

-- | Function type for radial basis functions.
--   Parameters are as follows:
--      (Float, Float)      : data point
--      -> Float            : radius sigma
--      -> (Float, Float)   : center point
type RBF = (Float, Float) -> Float -> (Float, Float) -> Float

-- | Gaussean radial basis function.
gauss :: RBF
gauss (x, y) sigma (cx, cy) =
    let norm = sqrt $ (x - cx)^2 + (y - cy)^2 in
        exp $ (norm / sigma)^2

-- | Classifies a data point p by using the given radial basis function rbf.
--   Additional parameters needed:
--      -> [Float]          : list of radii
--      -> [(Float, Float)] : list of center points
--      -> [Float]          : list of weights
classify :: RBF -> (Float, Float) -> [Float] -> [(Float, Float)] -> [Float] -> Float
classify rbf p = sum .... zipWith3 ((*) ... rbf p)
