module Bin(
    Octet,
    OctetString,
    file2os,
    os2file,
    str2os,
    os2str,
    octetSum,
    fromSum,
    octetSumB,
    fromSumB,
    fromSumB'
) where

import Data.Word
import Data.Char (ord, chr)
import Data.Bits
import Data.ByteString.Lazy as BSL
    hiding (map, concatMap, zipWith, reverse, take, drop, repeat, length)

-- | Defines an octet containing exactly 8 bit.
type Octet = Word8

-- | Defines an octet string (list of octets).
type OctetString = [Octet]

-- | Reads binary file and returns an octet string representation.
file2os :: FilePath -> IO OctetString
file2os file = BSL.readFile file >>= return . unpack

-- | Writes octet string to file.
os2file :: FilePath -> OctetString -> IO()
os2file file = BSL.writeFile file . pack

-- | Returns octet string representation of a string.
str2os :: String -> OctetString
str2os = map $ fromIntegral . ord

-- | Returns by octet string representated string.
os2str :: OctetString -> String
os2str = map $ chr . fromIntegral

-- | Returns integer which contains all octets of the given octet string.
octetSum :: OctetString -> Integer
octetSum =
    sum . zipWith (*) [ (2^e) | e <- [0,8..] ] . (map fromIntegral) . reverse

-- | Returns all octets that the integer contains.
fromSum :: Integer -> OctetString
fromSum 0 = [0]
fromSum x = reverse $ from x where
    from 0 = []
    from x = (fromInteger $ mod x 256) : (from (div x 256))

-- | Takes a block size bs (amount of octets) and an octet string, seperates the
--   string into blocks and returns a the octet sum for every block.
octetSumB :: Integer -> OctetString -> [Integer]
octetSumB bs os = map octetSum $ seperate bs os where
    seperate :: Integer -> OctetString -> [OctetString]
    seperate _ [] = []
    seperate bs os = let bs' = fromInteger bs in
        (take bs' os) : (seperate bs $ drop bs' os)

-- | Takes a block size bs (amount of octets) and an integer, seperates the
--   integer into blocks and returns an octet string for every block.
fromSumB :: Integer -> Integer -> [OctetString]
fromSumB bs x = map fromSum $ seperate bs x where
    seperate :: Integer -> Integer -> [Integer]
    seperate _ 0 = []
    seperate bs x = let b = 128 * bs in
        (mod x b) : (seperate bs $ div x b)

-- TODO Change 512 to a variable value.
-- | Takes a block size bs (amount of octets) and an integer, seperates the
--   integer into blocks and returns an octet string for every block.
--   Those octet strings will be concatenated and filled with 0s until it
--   reached a the size of 512 octets. 
fromSumB' :: Integer -> Integer -> OctetString
fromSumB' bs x = fill . concatMap fromSum $ reverse $ seperate bs x where
    seperate :: Integer -> Integer -> [Integer]
    seperate _ 0 = []
    seperate bs x = let b = 256 * bs in
        (mod x b) : (seperate bs $ div x b)
    fill :: OctetString -> OctetString
    fill os = let len = length os in (take (512 - len) (repeat 0)) ++ os

