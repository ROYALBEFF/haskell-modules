module SimpleRegex(
    Regex ((:.), (:+), Iter, Sym, Epsilon, Null),
    showStr,
    syms,
    parSyms,
    parSyms',
    iter,
    many,
    alphabet,
    fromAlphabet,
    check,
    wordsM,
    tailsM,
    wordsL,
    tailsL
) where

import Data.List (isPrefixOf, nub)

-- | Simple data type for regular expressions.
--   There are six constructors that can be used to create a regular expression.
--   (:.)       ->  sequentialisation of two regular expression
--                  (e.g. "a . b" or just "ab")
--   (:+)       ->  parallelisation of two regular expression
--                  (e.g. "a + b" or "a | b")
--   Iter       ->  iteration of one regex (e.g. (ab)*)
--   Sym        ->  embeds a value of type a by interpreting it as a symbol in
--                  the regular expression (e.g. 'a' -> "a")
--   Epsilon    ->  epsilon (empty word)
--   Null       ->  empty language
data Regex a = (Regex a) :. (Regex a) | (Regex a) :+ (Regex a) | Iter (Regex a)
               | Sym a | Epsilon | Null
infixr 6 :+
infixr 6 :.

-- | For representing a regular expression as an easy to read string.
instance Show a => Show (Regex a) where
    show ((Sym c) :. (Sym c')) = show c ++ show c'
    show ((Sym c) :. reg') = show c ++ show reg'
    show (reg :. (Sym c)) = show reg ++ show c
    show (reg :. reg') = "(" ++ show reg ++ ")(" ++ show reg' ++ ")"
    show (reg :+ reg') = show reg ++ "+" ++ show reg'
    show (Iter reg) = "(" ++ show reg ++ ")*"
    show (Sym c) = show c
    show Epsilon = "EPS"
    show Null = "NULL"

-- | Allows to check if two regular expression are equal.
--   Two regular expression can be unequal even if they describe
--   the same language.
instance Eq a => Eq (Regex a) where
    (reg :. reg') == (reg'' :. reg''') = reg == reg'' && reg' == reg'''
    (reg :+ reg') == (reg'' :+ reg''') = reg == reg'' && reg' == reg'''
    (Iter reg) == (Iter reg') = reg == reg'
    (Sym c) == (Sym c') = c == c'
    Epsilon == Epsilon = True
    Null == Null = True
    _ == _ = False

-- | fmap allows to apply a function f two every symbol in a regular expression.
instance Functor Regex where
    fmap f (reg :. reg') = (fmap f reg) :. (fmap f reg')
    fmap f (reg :+ reg') = (fmap f reg) :+ (fmap f reg')
    fmap f (Iter reg) = Iter $ fmap f reg
    fmap f (Sym c) = Sym $ f c
    fmap _ Epsilon = Epsilon
    fmap _ Null = Null

-- | To get a nicer string representation of a regular expression of characters
--   use this function instead of show.
showStr :: Regex Char -> String
showStr ((Sym c) :. (Sym c')) = c:c':""
showStr ((Sym c) :. reg') = c:(showStr reg')
showStr (reg :. (Sym c)) = showStr reg ++ (c:"")
showStr (reg :. reg') = "(" ++ showStr reg ++ ")(" ++ showStr reg' ++ ")"
showStr (reg :+ reg') = showStr reg ++ "+" ++ showStr reg'
showStr (Iter reg) = "(" ++ showStr reg ++ ")*"
showStr (Sym c) = c:""
showStr Epsilon = "EPS"
showStr Null = "NULL"

-- | Embeds all values in the given list.
--   Result is a sequentialisation of all those values.
--   e.g. syms "abc" = S 'a' :. S 'b' :. S 'c' (= "abc")
syms :: [a] -> Regex a
syms = foldl1 (:.) . map Sym

-- | Embeds all values in the given list.
--   Result is a parallelisation of all those values.
--   e.g. parSymy "abc" = S 'a' :+ S 'b' :+ S 'c' (= "a + b + c" to "a | b | c")
parSyms :: [a] -> Regex a
parSyms = foldl1 (:+) . map Sym

-- | Embeds all values of every list in the given list and paralleises the
--   resulting regular expressions.
parSyms' :: [[a]] -> Regex a
parSyms' = foldl1 (:+) . map syms

-- | Calls the Iter constructor.
iter :: Regex a -> Regex a
iter = Iter

-- | Iterates x times over a regular expression by sequentialising it x times.
many :: Regex a -> Int -> Regex a
many reg x = foldl1 (:.) $ take x $ repeat reg

-- | Returns a regular expression that describes the language that contains
--   every possible combination of values from the alphabet.
fromAlphabet :: [a] -> Regex a
fromAlphabet = iter . parSyms

-- | Returns a list containing all symbols a word from the described
--   language can be made of.
alphabet :: Eq a => Regex a -> [a]
alphabet = nub . alpha where
    alpha :: Regex a -> [a]
    alpha (reg :. reg') = alpha reg ++ alpha reg'
    alpha (reg :+ reg') = alpha reg ++ alpha reg'
    alpha (Iter reg) = alpha reg
    alpha (Sym c) = [c]
    alpha Epsilon = []
    alpha Null = []

-- | Validates if the word is element of the language that is described by
--   the regular expression given.
check :: Eq a => [a] -> Regex a -> Bool
--   Validate string in case of a sequentialisation
--   An empty string can always be created by two sequentialised iterations.
check [] ((Iter _) :. (Iter _)) = True
--   If the two sequentialised regular expression are no iterations
--   no empty word can be created.
check [] (_ :. _) = False
--   In all the other cases validate string with sequentialisation.
check str (reg :. reg') =
    any (\t -> check (fst t) reg && check (snd t) reg') $ subsequences str
--   Validate word with parallelisation
check str (reg :+ reg') = check str reg || check str reg'
--   An empty word can always be created by an iteration.
check [] (Iter _) = True
--   If the word only contains a single symbol we just have to validate if
--   the iterating regular expression can created this symbol.
check [c] (Iter reg) = check [c] reg
--   In all the other cases validate word with iteration.
check str ireg@(Iter reg) =
        check str reg ||
        (any (\t -> checkFst (fst t) && checkSnd (snd t)) $ subsequences str) where
            checkFst fstStr = check fstStr reg || check fstStr ireg
            checkSnd sndStr = check sndStr reg || check sndStr ireg 
--   Validate if the only symbol in the given word is embedded in the
--   regular expression.
check [c] (Sym c') = c == c'
--   If the word contains more than one symbol this validation will always
--   fail and return false.
check str (Sym _) = False
--   Validate word with epsilon. Only will return True if the passed word is
--   empty.
check [] Epsilon = True
check str Epsilon = False
--   Validate word with empty language.
check _ Null = False


-- | Divides a word in all possible combination of subwords that would result
--   in exactly this word by concating them.
--   e.g. subsequences "foo" = [("f", "oo"), ("fo", "o")]
subsequences :: [a] -> [([a], [a])]
subsequences str
    | len == 1  = [(str, [])]
    | otherwise = map (flip splitAt str) [1.. len-1] where
        len = length str

-- | Generates a list of all words that can be created with the given regular
--   expression, got a maximum length of n and got str as prefix.
tailsM :: Eq a => [a] -> Regex a -> Int -> [[a]]
tailsM str reg n = filter (isPrefixOf str) $ wordsM reg n 

-- | Generates a list of all words that can be created with the given regular
--   expression, got str as prefix and were exactly n symbols long.
tailsL :: Eq a => [a] -> Regex a -> Int -> [[a]]
tailsL str reg n = filter (isPrefixOf str) $ wordsL reg n

-- | Generates a list of all words that can be created with the given regular
--   expression and got a maximum length of n where n is the given integer.
--   Same as generateWords but gets rid of the first value of generateWords
--   result.
wordsM :: Regex a -> Int -> [[a]]
wordsM reg n = snd $ generateWords reg n

-- | Generates a list of all words that can be created with the given regular
--   expression and is exactly n symbols long.
wordsL :: Regex a -> Int -> [[a]]
wordsL reg n = filter ((== n) . length) $ wordsM reg n

-- | Generates a list of all words than can be created with the given regular
--   expression and got a maximum length of n where n is the given integer.
--   First value of the tuple is irrelevant for the result and is just needed
--   while generating the words and describes how many symbols are left
--   until reaching the maximum word length.
generateWords :: Regex a -> Int -> (Int, [[a]])
--   If the amount of missing symbols is 0 there are no more symbols to add
--   and the result will be an empty list.
generateWords _ 0 = (0, [])
--   Generate all words that are described by this sequentilisation.
generateWords (reg :. reg') n = let
    (n', ws)    = generateWords reg n
    (n'', ws')  = generateWords reg' n' in
        (n'', concatMap (\w -> map (++w) ws) ws')
--   Generate all words that are described by this parallelisation.
generateWords (reg :+ reg') n = let
    (n', ws)    = generateWords reg n
    (n'', ws')  = generateWords reg' n in
    (n', ws ++ ws')
--   Generate all words that are described by this iteration.
generateWords ireg@(Iter reg) n = let
    (n', ws)    = generateWords reg n
    (n'', ws')  = generateWords ireg n' in
        (n'', []:(combine ws ws')) where
            combine :: [[a]] -> [[a]] -> [[a]]
            combine [] _ = []
            combine (x:xs) ys = (x:[(x++y) | y <- ys]) ++ combine xs ys
--   In case that the regular expression just contains a single symbol
--   generate this symbol.
generateWords (Sym c) n = (n-1, [[c]])
--   In case that the regular expression is epsilon generate the empty word.
generateWords Epsilon n = (n, [[]])
--   In case that the regular expression is Null generate no words at all.
generateWords Null n = (n, [])

