module SimpleRegex.Rules where

import SimpleRegex

-- | A rule is a function that takes a word (list of some a-values) and decides
--   wether the word is valid.
type Rule a = [a] -> Bool

-- | Takes a replacement function rep that "replaces" symbols with other symbols.
--   Consider that the original symbol will NOT be deleted!
--   That means when a symbol x will be replaces with the symbol y every
--   occurence of x will be replaces by (x + y).
--   For a strict replacement simply use fmap.
replace :: Eq a => (a -> a) -> Regex a -> Regex a
replace rep (reg :. reg') = (replace rep reg) :. (replace rep reg')
replace rep (reg :+ reg') = (replace rep reg) :+ (replace rep reg')
replace rep (Iter reg) = Iter $ replace rep reg
replace rep s@(Sym c)
    | s == s'   = s
    | otherwise = s :+ s' where
        s' = Sym $ rep c 
replace _ Epsilon = Epsilon
replace _ Null = Null

-- | Applies a list of rules to a given list of words. Only those words that
--   follow the rules will be contained in the resulting list.
applyRules :: [Rule a] -> [[a]] -> [[a]]
applyRules rules ws = [ w | w <- ws, all id $ map ($ w) rules ]

