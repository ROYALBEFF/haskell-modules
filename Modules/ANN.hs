module ANN(
    fires,
    learn,
    check,
    run,
    rate
) where

import Control.Monad(foldM)
import Data.List(foldl')
import System.Random(randoms, newStdGen)
import Utils(btn)

-- | Checks if the neuron fires.
fires :: [Float] -> [Float] -> Bool
fires ws xs = (sum $ zipWith (*) ws xs) >= 0

-- | Returns a list of d random float values in an IO context.
randomWeights :: Int -> IO [Float]
randomWeights d = newStdGen >>= return . take (d+1) . randoms

-- | Perceptron learning.
--   Takes a value d that describes the dimension of the input vectors,
--   a learning rate alpha and
--   a set of input values to learn with.
--   The result is a vector of weights capsuled in IO.
learn :: Int -> Float -> [([Float], Bool)] -> IO [Float]
learn d alpha set = do
    ws <- randomWeights d
    putStrLn $ "[R] \t" ++ show ws
    ws' <- foldM (\ws' (xs,y) -> return $ zipWith (+) ws' $ let diff = (btn y) - (btn $ fires ws' xs) in map ((* (alpha * diff))) xs) ws set
    if check set ws' then putStrLn ("[DONE] \t" ++ show ws') >> return ws' else putStrLn ("[1] \t" ++ show ws') >> keeplearning alpha set ws' 2 where
        keeplearning alpha set ws i = do
            ws' <- foldM (\ws' (xs,y) -> return $ zipWith (+) ws' $ let diff = (btn y) - (btn $ fires ws' xs) in map ((* (alpha * diff))) xs) ws set
            if check set ws' then putStrLn ("[DONE] \t" ++ show ws') >> return ws' else putStrLn ('[' : show i ++ "] \t" ++ show ws') >> keeplearning alpha set ws' (i+1)

-- | Checks if the ANN with weights ws gives the correct answer for each vector
--   in the given set.
check :: [([Float], Bool)] -> [Float] -> Bool
check set ws = foldl (\acc (xs,y) -> acc && fires ws xs == y) True set

-- | Calculates the relative amount of correct answers for a given set of values
--   and a given vector of weights.
rate :: [([Float], Bool)] -> [Float] -> Float
rate xs ws = count / (fromIntegral $ length xs) where
    count = foldl' (\c (x,y) -> btn (y == (fires ws x)) + c) 0 xs

-- | Gathers knowledge with input data trainset and a learn factor of alpha.
--   The elements in trainset are d-dimensional. When the learning phase is done
--   the ANN will be tested with new input data testset.
--   The function returns the relative amount of correct answers capsuled in IO.
run :: Int -> Float -> [([Float], Bool)] -> [([Float], Bool)] -> IO Float
run d alpha trainset testset = do
    ws <- learn d alpha trainset
    return $ rate testset ws
